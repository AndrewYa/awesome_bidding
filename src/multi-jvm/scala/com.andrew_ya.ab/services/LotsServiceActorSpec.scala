package com.andrew_ya.ab.services

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import akka.cluster.Cluster
import akka.cluster.ddata.DistributedData
import akka.cluster.ddata.Replicator.{GetReplicaCount, ReplicaCount}
import akka.remote.testconductor.RoleName
import akka.remote.testkit.{MultiNodeConfig, MultiNodeSpec}
import akka.testkit.ImplicitSender
import com.andrew_ya.ab.STMultiNodeSpec
import com.andrew_ya.ab.domain.{Lot, LotRequest, Lots}
import com.andrew_ya.ab.services.ClientServiceActor.LotNotify
import com.andrew_ya.ab.services.LotsServiceActor._
import com.typesafe.config.ConfigFactory
import org.scalatest.Inside

import scala.concurrent.duration._


object LotsServiceActorSpec extends MultiNodeConfig {
  val node1 = role("node-1")
  val node2 = role("node-2")
  val node3 = role("node-3")

  commonConfig(ConfigFactory.parseString(
    """
    akka.loglevel = INFO
    akka.actor.provider = "cluster"
    akka.log-dead-letters-during-shutdown = off
    """))
}


class LotsServiceActorSpecMultiJvmNode1 extends LotsServiceActorSpec

class LotsServiceActorSpecMultiJvmNode2 extends LotsServiceActorSpec

class LotsServiceActorSpecMultiJvmNode3 extends LotsServiceActorSpec

class LotsServiceActorSpec extends MultiNodeSpec(LotsServiceActorSpec) with STMultiNodeSpec with ImplicitSender with Inside {

  import LotsServiceActorSpec._

  override def initialParticipants = roles.size

  val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")


  val cluster = Cluster(system)
  val lotsServiceActor = system.actorOf(LotsServiceActor.props)

  def join(from: RoleName, to: RoleName): Unit = {
    runOn(from) {
      cluster join node(to).address
    }
    enterBarrier(from.name + "-joined")
  }


  "Demo of a replicated lots service" should {
    "join cluster" in within(20.seconds) {
      join(node1, node1)
      join(node2, node1)
      join(node3, node1)

      awaitAssert {
        DistributedData(system).replicator ! GetReplicaCount
        expectMsg(ReplicaCount(roles.size))
      }
      enterBarrier("after-1")
    }


    "handle updates OrderCreated notifies " in within(20.seconds) {
      runOn(node2) {
        lotsServiceActor ! OrderCreated(lotId = "111", orderId = 1L)
        lotsServiceActor ! OrderCreated(lotId = "555", orderId = 4L)
        lotsServiceActor ! OrderCreated(lotId = "666", orderId = 5L)
        lotsServiceActor ! OrderCreated(lotId = "111", orderId = 2L)
      }
      enterBarrier("updates-done")

      awaitAssert {
        lotsServiceActor ! GetOrdersIdByLotId("111")
        val orders = expectMsgType[OrdersByLot]
        orders.orderIds should be(Set(1, 2))
      }

      awaitAssert {
        lotsServiceActor ! GetOrdersIdByLotId("555")
        val orders = expectMsgType[OrdersByLot]
        orders.orderIds should be(Set(4))
      }
      enterBarrier("after-2")
    }


    "handle create lots " in within(20.seconds) {
      runOn(node1) {
        lotsServiceActor ! LotRequest(clientId = 11, price = 0.42, expirationDate = LocalDateTime.parse("2018-05-01 23:00:00", formatter))
        lotsServiceActor ! LotRequest(clientId = 11, price = 0.55, expirationDate = LocalDateTime.parse("2018-05-02 23:00:00", formatter))

      }
      enterBarrier("updates-done")

      awaitAssert {
        lotsServiceActor ! GetAllLots
        val lots = expectMsgType[Lots]


        lots.lots.map(_.clientId) should contain theSameElementsAs List(11, 11)
        lots.lots.map(_.price) should contain theSameElementsAs List(0.55, .42)
        lots.lots.map(_.expirationDate) should contain theSameElementsAs List(LocalDateTime.parse("2018-05-01 23:00:00", formatter),
          LocalDateTime.parse("2018-05-02 23:00:00", formatter))

      }
      enterBarrier("after-3")
    }

    "filter lots by dates" in within(10.seconds) {

      runOn(node3) {
        lotsServiceActor ! LotRequest(clientId = 11, price = 0.42, expirationDate = LocalDateTime.parse("2018-05-01 23:00:00", formatter))
        lotsServiceActor ! LotRequest(clientId = 33, price = 0.55, expirationDate = LocalDateTime.parse("2018-05-02 23:00:00", formatter))
        lotsServiceActor ! LotRequest(clientId = 44, price = 0.55, expirationDate = LocalDateTime.parse("2018-05-03 23:00:00", formatter))
        lotsServiceActor ! LotRequest(clientId = 22, price = 0.55, expirationDate = LocalDateTime.parse("2018-05-04 23:00:00", formatter))
        lotsServiceActor ! LotRequest(clientId = 11, price = 0.55, expirationDate = LocalDateTime.parse("2018-05-05 23:00:00", formatter))

      }
      enterBarrier("updates-done")


      awaitAssert {
        lotsServiceActor ! GetLots(LocalDateTime.parse("2018-05-03 19:00:00", formatter), None)
        val lots = expectMsgType[Lots]

        lots.lots.map(_.clientId) should contain theSameElementsAs List(11, 22, 44)
        lots.lots.map(_.price) should contain theSameElementsAs List(0.55, .55, 0.55)
        lots.lots.map(_.expirationDate) should contain theSameElementsAs List(LocalDateTime.parse("2018-05-03 23:00:00", formatter),
          LocalDateTime.parse("2018-05-04 23:00:00", formatter), LocalDateTime.parse("2018-05-05 23:00:00", formatter))

      }
      enterBarrier("after-4")

    }


    "filter lots by dates and price" in within(10.seconds) {

      runOn(node3) {
        lotsServiceActor ! LotRequest(clientId = 11, price = 0.42, expirationDate = LocalDateTime.parse("2018-06-01 23:00:00", formatter))
        lotsServiceActor ! LotRequest(clientId = 33, price = 0.55, expirationDate = LocalDateTime.parse("2018-06-02 23:00:00", formatter))
        lotsServiceActor ! LotRequest(clientId = 44, price = 0.75, expirationDate = LocalDateTime.parse("2018-06-03 23:00:00", formatter))
        lotsServiceActor ! LotRequest(clientId = 22, price = 0.55, expirationDate = LocalDateTime.parse("2018-06-04 23:00:00", formatter))
        lotsServiceActor ! LotRequest(clientId = 11, price = 0.80, expirationDate = LocalDateTime.parse("2018-06-05 23:00:00", formatter))
        lotsServiceActor ! LotRequest(clientId = 11, price = 0.55, expirationDate = LocalDateTime.parse("2018-06-06 23:00:00", formatter))

      }
      enterBarrier("updates-done")


      awaitAssert {
        lotsServiceActor ! GetLots(LocalDateTime.parse("2018-06-03 19:00:00", formatter), Some(0.63))
        val lots = expectMsgType[Lots]

        lots.lots.map(_.clientId) should contain theSameElementsAs List(11, 22)
        lots.lots.map(_.price) should contain theSameElementsAs List(0.55, .55)
        lots.lots.map(_.expirationDate) should contain theSameElementsAs List(LocalDateTime.parse("2018-06-04 23:00:00", formatter),
          LocalDateTime.parse("2018-06-06 23:00:00", formatter))

      }
      enterBarrier("after-5")

    }

  }
}
package com.andrew_ya.ab.services


import scala.concurrent.duration._
import akka.cluster.Cluster
import akka.cluster.ddata.DistributedData
import akka.cluster.ddata.Replicator.{GetReplicaCount, ReplicaCount}
import akka.remote.testconductor.RoleName
import akka.remote.testkit.{MultiNodeConfig, MultiNodeSpec}
import akka.testkit.ImplicitSender
import com.andrew_ya.ab.STMultiNodeSpec
import com.andrew_ya.ab.domain.{Order, Orders}
import com.typesafe.config.ConfigFactory


object ClientServiceActorSpec extends MultiNodeConfig {
  val node1 = role("node-1")
  val node2 = role("node-2")
  val node3 = role("node-3")

  commonConfig(ConfigFactory.parseString(
    """
    akka.loglevel = INFO
    akka.actor.provider = "cluster"
    akka.log-dead-letters-during-shutdown = off
    """))
}


class ClientServiceActorSpecMultiJvmNode1 extends ClientServiceActorSpec

class ClientServiceActorSpecMultiJvmNode2 extends ClientServiceActorSpec

class ClientServiceActorSpecMultiJvmNode3 extends ClientServiceActorSpec

class ClientServiceActorSpec extends MultiNodeSpec(ClientServiceActorSpec) with STMultiNodeSpec with ImplicitSender {

  import ClientServiceActorSpec._
  import ClientServiceActor._

  override def initialParticipants = roles.size


  val cluster = Cluster(system)
  val clientServiceActor = system.actorOf(ClientServiceActor.props)

  def join(from: RoleName, to: RoleName): Unit = {
    runOn(from) {
      cluster join node(to).address
    }
    enterBarrier(from.name + "-joined")
  }



  "Demo of a replicated client service" should {
    "join cluster" in within(20.seconds) {
      join(node1, node1)
      join(node2, node1)
      join(node3, node1)

      awaitAssert {
        DistributedData(system).replicator ! GetReplicaCount
        expectMsg(ReplicaCount(roles.size))
      }
      enterBarrier("after-1")
    }


    "handle updates directly after start" in within(20.seconds) {
      runOn(node2) {
        clientServiceActor ! ClientServiceActor.AddOrder(lotId = "111", clientId = 1L)
        clientServiceActor ! ClientServiceActor.AddOrder(lotId = "555", clientId = 1L)
        clientServiceActor ! ClientServiceActor.AddOrder(lotId = "666", clientId = 1L)
        clientServiceActor ! ClientServiceActor.AddOrder(lotId = "111", clientId = 2L)
      }
      enterBarrier("updates-done")

      awaitAssert {
        clientServiceActor ! ClientServiceActor.GetOrders(1)
        val orders = expectMsgType[Orders]
        orders.orders should be(List(Order(111,"111"), Order(111,"555"),  Order(111,"666")))
      }

      enterBarrier("after-2")
    }


  }


}

package com.andrew_ya.ab.integration

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import akka.actor.ActorRef
import akka.cluster.Cluster
import akka.cluster.ddata.DistributedData
import akka.cluster.ddata.Replicator.{GetReplicaCount, ReplicaCount}
import akka.remote.testconductor.RoleName
import akka.remote.testkit.{MultiNodeConfig, MultiNodeSpec}
import akka.testkit.ImplicitSender
import akka.util.Timeout
import com.andrew_ya.ab.STMultiNodeSpec
import com.andrew_ya.ab.domain.{LotRequest, Order, Orders}
import com.andrew_ya.ab.services.{ClientServiceActor, LotsServiceActor, ServiceApi}
import com.typesafe.config.ConfigFactory

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._


object IntegrationSpec extends MultiNodeConfig {
  val node1 = role("node-1")
  val node2 = role("node-2")
  val node3 = role("node-3")

  commonConfig(ConfigFactory.parseString(
    """
    akka.loglevel = INFO
    akka.actor.provider = "cluster"
    akka.log-dead-letters-during-shutdown = off
    """))
}


class ClientServiceActorSpecMultiJvmNode1 extends IntegrationSpec

class ClientServiceActorSpecMultiJvmNode2 extends IntegrationSpec

class ClientServiceActorSpecMultiJvmNode3 extends IntegrationSpec

class IntegrationSpec extends MultiNodeSpec(IntegrationSpec) with STMultiNodeSpec with ImplicitSender {

  val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")

  import IntegrationSpec._

  override def initialParticipants = roles.size


  val cluster = Cluster(system)


  object Service extends ServiceApi {

    override def createClientsService(): ActorRef = system.actorOf(ClientServiceActor.props)
    override def createLotsService(): ActorRef = system.actorOf(LotsServiceActor.props)

    override implicit def executionContext: ExecutionContext = system.dispatcher
    override implicit def requestTimeout: Timeout = 5.seconds
  }

  def join(from: RoleName, to: RoleName): Unit = {
    runOn(from) {
      cluster join node(to).address
    }
    enterBarrier(from.name + "-joined")
  }



  "Demo of a replicated  service" should {
    "join cluster" in within(20.seconds) {
      join(node1, node1)
      join(node2, node1)
      join(node3, node1)

      awaitAssert {
        DistributedData(system).replicator ! GetReplicaCount
        expectMsg(ReplicaCount(roles.size))
      }
      enterBarrier("after-1")
    }


    "handle updates directly after start" in within(20.seconds) {
      runOn(node2) {

        Service.createLot(request = LotRequest(clientId = 11, price = 0.42, expirationDate = LocalDateTime.parse("2018-05-01 23:00:00", formatter))

      }
      enterBarrier("updates-done")

      awaitAssert {

      }

      enterBarrier("after-2")
    }


  }


}

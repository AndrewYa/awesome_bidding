package com.andrew_ya.ab.rest

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model.{ContentTypes, HttpRequest, MessageEntity, StatusCodes}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.andrew_ya.ab.domain._
import com.andrew_ya.ab.services.LotsService
import org.scalatest.concurrent.ScalaFutures._
import org.scalatest.{Matchers, WordSpec}

import scala.concurrent.Future


//Разместить лот от имени клиента с указанием начальной цены и времени торга
//Получить список не просроченных лотов с начальной ценой, не превышающей заданную
//Разместить заявку на лот от имени клиента
//Получить список активных заявок клиента
//Получить список заявок по лоту

class LotsRoutesSpec extends WordSpec with Matchers with EntityMarshalling with ScalatestRouteTest {


  val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")

  implicit object MockLotsService extends LotsService {
    override def createLot(request: LotRequest): Future[Lot] = {

      if (request.clientId == 111) {
        Future.successful((Lot("42", 111, 45.45, LocalDateTime.parse("2018-05-30 23:00:00", formatter))))
      } else {
        Future.failed(new RuntimeException)
      }

    }

    override def findLotsByPrice(price: Double): Future[Option[Lots]] = {

      Future.successful(if (price == 0.5) {
        Some(
          Lots(List(
            Lot("42", 111, 0.1, LocalDateTime.parse("2018-05-30 23:00:00", formatter)),
            Lot("42", 111, 0.2, LocalDateTime.parse("2018-05-30 23:00:00", formatter)),
            Lot("42", 111, 0.3, LocalDateTime.parse("2018-05-30 23:00:00", formatter))
          ))
        )

      } else {
        None
      })

    }

    override def getOrders(lotId: Long): Future[Option[Orders]] = {
      Future.successful(
        if (lotId == 111) {
          Some(Orders(List(Order(12, "111"), Order(13, "111"), Order(14, "111"))))
        } else  {
          None
        }
      )
    }


  }


  val routes = Route.seal(LotsRoute())


  "LotRoutes" should {
    "return no lots if no present (GET /lots)" in {
      val request = HttpRequest(uri = "/lots")

      request ~> routes ~> check {
        status should ===(StatusCodes.NotFound)
      }
    }


    "create lot (POST /lots/new)" in {


      val lotRequest = LotRequest(111, 45.45, LocalDateTime.parse("2018-05-30 23:00:00", formatter))
      val userEntity = Marshal(lotRequest).to[MessageEntity].futureValue

      val request = Post("/lots/new").withEntity(userEntity)

      request ~> routes ~> check {
        status should ===(StatusCodes.Created)

        // we expect the response to be json:
        contentType should ===(ContentTypes.`application/json`)

        // and we know what message we're expecting back:
        entityAs[String] should ===("""{"id":"42","clientId":111,"price":45.45,"expirationDate":"2018-05-30 23:00:00"}""")
      }

    }

    "return lots with lower or equal specified price (GET /lots/all?price=0.5)" in {
      // note that there's no need for the host part in the uri:
      val request = HttpRequest(uri = "/lots/all?price=0.5")


      request ~> routes ~> check {
        status should ===(StatusCodes.OK)
        contentType should ===(ContentTypes.`application/json`)
        entityAs[String] should ===("""{"lots":[{"id":"42","clientId":111,"price":0.1,"expirationDate":"2018-05-30 23:00:00"},{"id":"42","clientId":111,"price":0.2,"expirationDate":"2018-05-30 23:00:00"},{"id":"42","clientId":111,"price":0.3,"expirationDate":"2018-05-30 23:00:00"}]}""")
      }
    }


    "return orders by lot id (GET /lots/111/orders)" in {

      val request = HttpRequest(uri = "/lots/111/orders")


      request ~> routes ~> check {
        status should ===(StatusCodes.OK)
        contentType should ===(ContentTypes.`application/json`)
        entityAs[String] should ===("""{"orders":[{"id":12,"lotId":"111"},{"id":13,"lotId":"111"},{"id":14,"lotId":"111"}]}""")
      }


    }


  }

}

package com.andrew_ya.ab.rest


import com.andrew_ya.ab.services.ClientService
import akka.http.scaladsl.model.{ContentTypes, HttpMethods, HttpRequest, StatusCodes}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.andrew_ya.ab.domain._
import org.scalatest.{Matchers, WordSpec}

import scala.concurrent.Future


class ClientsRoutesSpec  extends WordSpec with Matchers with ScalatestRouteTest {


  implicit object MockClientService extends ClientService {

    override def getActiveOrders(clientId: Long): Future[Option[Orders]] = {

      Future.successful(if (clientId == 4554) {
        Some(Orders(List(Order(1, "1"), Order(2, "2"), Order(3, "3"))))
      } else if (clientId == 233) {
        Some(Orders(List.empty[Order]))
      } else {
        None
      })

    }
  }



  val routes  = Route.seal(ClientsRoute())


  "ClientRoutes" should {
    "return no orders if no present (GET /clients/233/orders)" in {

      val request = HttpRequest(HttpMethods.GET, uri = "/clients/233/orders")

      request ~> routes ~> check {
        status shouldEqual StatusCodes.OK
        contentType shouldEqual ContentTypes.`application/json`
        responseAs[String] shouldEqual """{"orders":[]}"""
      }
    }

    "return existing orders (GET /clients/4554/orders)" in {
      // note that there's no need for the host part in the uri:
      val request = HttpRequest(HttpMethods.GET, uri = "/clients/4554/orders")

      request ~> routes ~> check {
        status shouldEqual StatusCodes.OK
        contentType shouldEqual ContentTypes.`application/json`
        responseAs[String] shouldEqual """{"orders":[{"id":1,"lotId":"1"},{"id":2,"lotId":"2"},{"id":3,"lotId":"3"}]}"""
      }
    }

    "return NOT FOUND if resource not present (GET /clients/4554)" in {

      val request = HttpRequest(HttpMethods.GET, uri = "/clients/4554")

      request ~> routes ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }


    "return NOT FOUND if client id does not exist present (GET /clients/999)" in {

      val request = HttpRequest(HttpMethods.GET, uri = "/clients/999")

      request ~> routes ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }


    "return BAD REQUEST if client Id not present (GET /clients)" in {

      val request = HttpRequest(HttpMethods.GET, uri = "/clients")

      request ~> routes ~> check {
        status shouldEqual StatusCodes.BadRequest
      }
    }


  }




}

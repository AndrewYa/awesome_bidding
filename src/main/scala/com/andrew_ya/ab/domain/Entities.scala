package com.andrew_ya.ab.domain

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.{DefaultJsonProtocol, DeserializationException, JsString, JsValue, RootJsonFormat}

sealed trait Entities

case class Lot(id: String, clientId: Long,  price: Double, expirationDate: LocalDateTime) extends Entities
case class Client(id: Long) extends Entities
case class Order(id: Long, lotId: String) extends Entities //todo isActive ?


case class OrderRequest(lotId: Long)
case class LotRequest(clientId: Long, price: Double, expirationDate: LocalDateTime)


case class Orders(orders: List[Order])
case class Lots(lots: List[Lot])


trait EntityMarshalling extends SprayJsonSupport with DefaultJsonProtocol {

  implicit object DateJsonFormat extends RootJsonFormat[LocalDateTime] {

    val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")

    override def write(obj: LocalDateTime) = JsString(formatter.format(obj))

    override def read(json: JsValue): LocalDateTime = json match {
      case JsString(s) => LocalDateTime.parse(s, formatter)
      case _ => throw new DeserializationException("Error info you want here ...")
    }

  }


  implicit val lotFormat = jsonFormat4(Lot)
  implicit val clientFormat = jsonFormat1(Client)
  implicit val orderFormat = jsonFormat2(Order)
  implicit val ordersFormat = jsonFormat1(Orders)
  implicit val lotsFormat = jsonFormat1(Lots)

  implicit val orderRequestFormat = jsonFormat1(OrderRequest)
  implicit val lotRequestFormat = jsonFormat3(LotRequest)

}
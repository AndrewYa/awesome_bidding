package com.andrew_ya.ab.rest


import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{MethodRejection, RejectionHandler, Route}
import com.andrew_ya.ab.domain.{EntityMarshalling, LotRequest}
import com.andrew_ya.ab.services.LotsService


object LotsRoute extends EntityMarshalling {

  def apply()(implicit lotsService: LotsService): Route = {
    def rejectionHandler =
      RejectionHandler.newBuilder()
        .handleAll[MethodRejection] { _ =>
        complete(HttpResponse(StatusCodes.MethodNotAllowed))
      }
        .handleNotFound {
          complete(HttpResponse(StatusCodes.NotFound))
        }
        .result()

    val route = handleRejections(rejectionHandler) {
      pathPrefix("lots") {
        post {
          path("new") {
            entity(as[LotRequest]) { request =>
              onSuccess(lotsService.createLot(request)) { lot =>
                //                  _.fold(complete(BadRequest))(lot => complete(StatusCodes.Created -> lot))
                //                  todo add error handling
                complete(StatusCodes.Created -> lot)
              }
            }
          }
        } ~
          get {
            pathPrefix("all") {
              pathEndOrSingleSlash {
                parameters('price.as[Double]) { price =>
                  onSuccess(lotsService.findLotsByPrice(price)) {
                    _.fold(complete(NotFound))(lots => complete(lots))
                  }
                }
              }
            } ~
                pathPrefix(LongNumber) { lotId =>
                  path("orders") {
                    onSuccess(lotsService.getOrders(lotId)) {
                      _.fold(complete(NotFound))(orders => complete(orders))
                    }
                  }
                }
            }
          }
      }



    route

  }
}

package com.andrew_ya.ab.rest

import akka.http.scaladsl.model.{HttpEntity, HttpResponse, StatusCodes}
import akka.http.scaladsl.server.{MethodRejection, RejectionHandler, Route}
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import com.andrew_ya.ab.domain.{EntityMarshalling, LotRequest, OrderRequest, Orders}
import com.andrew_ya.ab.services.ClientService

object ClientsRoute extends EntityMarshalling {

  def apply()(implicit clientService: ClientService): Route = {
    def rejectionHandler =
      RejectionHandler.newBuilder()
        .handleAll[MethodRejection] { _ =>
        complete(HttpResponse(StatusCodes.MethodNotAllowed))
      }
        .handleNotFound {
          complete(HttpResponse(StatusCodes.NotFound))
        }
        .result()

    val route =
      handleRejections(rejectionHandler) {
      pathPrefix("clients") {
        pathPrefix(LongNumber) { id =>
            get {
              path("orders") {
                onSuccess(clientService.getActiveOrders(id))  {
                _.fold(complete(StatusCodes.NotFound))(orders => complete(StatusCodes.OK, orders))
                }
              }
            }
          }
        } ~ path(Segment) { seq =>
          get {
            complete(HttpResponse(StatusCodes.BadRequest))
          }

        }
      }

    route
  }

}

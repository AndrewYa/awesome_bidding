package com.andrew_ya.ab.services

import scala.concurrent.duration._
import akka.actor.{Actor, ActorRef, Props}
import akka.cluster.Cluster
import akka.cluster.ddata._
import akka.cluster.ddata.Replicator.{ReadMajority, WriteMajority}
import akka.util.Timeout
import com.andrew_ya.ab.domain.{Order, Orders}

class ClientServiceActor extends Actor {

  import ClientServiceActor._
  import akka.cluster.ddata.Replicator._

  val replicator = DistributedData(context.system).replicator
  implicit val cluster = Cluster(context.system)

  override def receive: Receive = receiveAddOrder
    .orElse[Any, Unit](receiveGetOrders)



  val key = ORMultiMapKey[Long, Order]("OrderMap")

  def receiveGetOrders: Receive = {
    case  GetOrders(clientId) =>

      replicator ! Get(key, readMajority, Some(sender(), clientId))

    case g @ GetSuccess(key: ORMultiMapKey[Long, Order], Some(replyTo: (ActorRef, Long))) =>

      val data = g.get(key)
      val orders = Orders(data.getOrElse(replyTo._2, Set.empty[Order]).toList)
      replyTo._1 ! orders

    case NotFound(key: ORMultiMapKey[Long, Order], Some(replyTo: (ActorRef, Long))) =>
      replyTo._1 ! Orders(List.empty[Order])

    case GetFailure(key, Some(replyTo: (ActorRef, Long))) =>
      // ReadMajority failure, try again with local read
      replicator ! Get(key, ReadLocal, Some(replyTo))
  }


  def receiveAddOrder: Receive = {
    case cmd @ AddOrder(clientId, lotId) =>

      val update = Update(key, ORMultiMap.empty[Long, Order], writeMajority, Some(cmd)) {
        orders => updateOrders(clientId, Order(111L, lotId), orders)
      }

      replicator ! update
  }


  def updateOrders(clientId: Long, order: Order, orders: ORMultiMap[Long, Order]): ORMultiMap[Long, Order] = {
    orders.get(clientId) match {
      case Some(clientOrders) => orders + (clientId -> (clientOrders + order))
      case None => orders + (clientId -> Set(order))
    }
  }


}


object ClientServiceActor {

//  def props(implicit timeout: Timeout): Props = Props[ClientServiceActor]
  def props: Props = Props[ClientServiceActor]

  private val timeout = 3.seconds
  private val readMajority = ReadMajority(timeout)
  private val writeMajority = WriteMajority(timeout)



  case class OrderNotify(lotId: Long, orderId: Long)
  case class LotNotify(lotId: String)


  case class AddOrder(clientId: Long, lotId: String)
  case class GetOrders(clientId: Long)


}

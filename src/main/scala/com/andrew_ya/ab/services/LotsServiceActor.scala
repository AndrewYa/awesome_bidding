package com.andrew_ya.ab.services

import java.time.LocalDateTime
import java.util.UUID

import scala.concurrent.duration._
import akka.actor.{Actor, ActorRef, Address, Props}
import akka.cluster.Cluster
import akka.cluster.ddata.{DistributedData, LWWRegister, ORMultiMap, ORMultiMapKey}
import akka.cluster.ddata.Replicator.{ReadMajority, WriteMajority}
import com.andrew_ya.ab.domain.{Lot, LotRequest, Lots, Order}
import com.andrew_ya.ab.services.ClientServiceActor.LotNotify
import com.andrew_ya.ab.services.LotsServiceActor.GetAllLots


class LotsServiceActor extends Actor {


  import LotsServiceActor._
  import akka.cluster.ddata.Replicator._

  val replicator = DistributedData(context.system).replicator
  implicit val cluster = Cluster(context.system)

  //Разместить лот от имени клиента с указанием начальной цены и времени торга
  //Получить список не просроченных лотов с начальной ценой, не превышающей заданную
  //Разместить заявку на лот от имени клиента
  //Получить список активных заявок клиента
  //Получить список заявок по лоту

  type LotId = String
  type OrderId = Long


  val lotOrderIndexKey = ORMultiMapKey[LotId, OrderId]("lotOrderIndex")

  val lotsKey = ORMultiMapKey[LotId, Lot]("lots")


  val earliestDate = LWWRegister(LocalDateTime.now) //todo Add timezone


  def nodeKey(address: Address): String = address.host.get + ":" + address.port.get
  def getUuid: String = UUID.randomUUID().toString  + "-" + nodeKey(cluster.selfAddress)

  override def receive: Receive = orderHandle
    .orElse[Any, Unit](receiveAddLot)
    .orElse[Any, Unit](receiveGetLots)
    .orElse[Any, Unit](receiveGetAllLots)


  def orderHandle: Receive = {
    case cmd@OrderCreated(lotId, orderId) =>


      val update = Update(lotOrderIndexKey, ORMultiMap.empty[LotId, OrderId], writeMajority, Some(cmd)) {
        orderIndex => updateKV(lotId, orderId, orderIndex)
      }

      replicator ! update

    case a@GetOrdersIdByLotId(lotId) =>

      replicator ! Get(lotOrderIndexKey, readMajority, Some((sender(), lotId)))


    case g@GetSuccess(lotOrderIndexKey: ORMultiMapKey[LotId, OrderId], Some(replyTo: (ActorRef, LotId))) =>

      val data = g.get(lotOrderIndexKey)
      val orders = OrdersByLot(data.getOrElse(replyTo._2, Set.empty[OrderId]))
      replyTo._1 ! orders


    case NotFound(lotOrderIndexKey, Some(replyTo: (ActorRef, OrderId))) =>
      replyTo._1 ! OrdersByLot(Set.empty[OrderId])


    case GetFailure(lotOrderIndexKey, Some(replyTo: (ActorRef, OrderId))) =>
      // ReadMajority failure, try again with local read
      replicator ! Get(lotOrderIndexKey, ReadLocal, Some(replyTo))


  }

  def receiveGetAllLots: Receive = {
    case GetAllLots =>

      replicator ! Get(lotsKey, readMajority, Some(sender()))

    case g@GetSuccess(key: ORMultiMapKey[LotId, Lot], Some(replyTo: ActorRef)) =>

      val data = g.get(key)
      val lots = Lots(data.entries.values.flatten.toList)
      replyTo ! lots

    case NotFound(key: ORMultiMapKey[LotId, Lot], Some(replyTo: ActorRef)) =>
      replyTo ! Lots(List.empty[Lot])

    case GetFailure(key: ORMultiMapKey[LotId, Lot], Some(replyTo: ActorRef)) =>
      // ReadMajority failure, try again with local read
      replicator ! Get(key: ORMultiMapKey[LotId, Lot], ReadLocal, Some(replyTo))
  }



  def receiveGetLot: Receive = {

    case GetLot(lotId, replyTo) =>
      replicator ! Get(lotsKey, readMajority, Some((replyTo, lotId)))

    case g@GetSuccess(key: ORMultiMapKey[LotId, Lot], Some(replyTo: (ActorRef, String))) =>

      val data = g.get(key)
      replyTo._1 ! data.getOrElse(replyTo._2, Set.empty[Lot])

    case NotFound(key: ORMultiMapKey[LotId, Lot], Some(replyTo: (ActorRef, String))) =>
      replyTo._1 ! Set.empty[Lot]

    case GetFailure(key: ORMultiMapKey[LotId, Lot], Some(replyTo: (ActorRef, String))) =>
      // ReadMajority failure, try again with local read
      replicator ! Get(key: ORMultiMapKey[LotId, Lot], ReadLocal, Some(replyTo))

  }


  def receiveGetLots: Receive = {
    case GetLots(currentDate, Some(price)) =>
      replicator ! Get(lotsKey, readMajority, Some(sender(), currentDate, Some(price)))

    case GetLots(currentDate, None) =>
      replicator ! Get(lotsKey, readMajority, Some(sender(), currentDate, None))

    case g@GetSuccess(key: ORMultiMapKey[LotId, Lot], Some(replyTo: (ActorRef, LocalDateTime, Option[Double]))) =>

//      todo this ineffective logic may be replace with partitioning by dates and prices
      val data = g.get(key)
      val allLots = data.entries.values.flatten.toList
      val activeLots = allLots.filter{case Lot(_, _, _, expDate) => expDate.compareTo(replyTo._2) > 0}

      val filteredLots = replyTo._3 match {
        case Some(pr) => activeLots.filter{case Lot(_, _, price, _) => pr >= price}
        case None => activeLots
      }

      replyTo._1 ! Lots(filteredLots)

    case NotFound(key: ORMultiMapKey[LotId, Lot], Some(replyTo: (ActorRef, LocalDateTime, Option[Double]))) =>
      replyTo._1 ! Lots(List.empty[Lot])

    case GetFailure(key: ORMultiMapKey[LotId, Lot], Some(replyTo: (ActorRef, LocalDateTime, Option[Double]))) =>
      // ReadMajority failure, try again with local read
      replicator ! Get(key: ORMultiMapKey[LotId, Lot], ReadLocal, Some(replyTo))
  }


  def receiveAddLot: Receive = {
    case cmd@LotRequest(clientId: Long, price: Double, expirationDate: LocalDateTime) =>

      val lotId = getUuid

      val update = Update(lotsKey, ORMultiMap.empty[LotId, Lot], writeMajority, Some(cmd)) {
        lots => updateKV(lotId, Lot(lotId, clientId, price, expirationDate), lots)
      }

      replicator ! update
      self ! GetLot(lotId, sender())
  }

  def updateKV[K, V](key: K, value: V, values: ORMultiMap[K, V]): ORMultiMap[K, V] = {
    values.get(key) match {
      case Some(existedValues) => values + (key -> (existedValues + value))
      case None => values + (key -> Set(value))
    }
  }


}


object LotsServiceActor {

  def props: Props = Props[LotsServiceActor]

  private val timeout = 3.seconds
  private val readMajority = ReadMajority(timeout)
  private val writeMajority = WriteMajority(timeout)


  case class AddLot()


  case object GetAllLots

  case class GetLots(currentDate: LocalDateTime, price: Option[Double])

  case class GetLot(lotId: String, replyTo: ActorRef)

  case class GetOrdersIdByLotId(lotId: String)

  case class OrdersByLot(orderIds: Set[Long])

  case class OrderCreated(lotId: String, orderId: Long)

}

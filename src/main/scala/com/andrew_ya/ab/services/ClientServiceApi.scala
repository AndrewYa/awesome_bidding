package com.andrew_ya.ab.services

import akka.actor.ActorSystem
import akka.util.Timeout
import com.andrew_ya.ab.domain.Orders

import scala.concurrent.Future

class ClientServiceApi(system: ActorSystem, timeout: Timeout) extends ClientService {


  implicit val requestTimeout = timeout
  implicit def executionContext = system.dispatcher

  def createBoxOffice = system.actorOf(ClientServiceActor.props)



  override def getActiveOrders(clientId: Long): Future[Option[Orders]] = ???



}

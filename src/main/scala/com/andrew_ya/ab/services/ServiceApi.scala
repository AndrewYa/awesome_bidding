package com.andrew_ya.ab.services

import akka.actor.{ActorRef, ActorSystem}
import akka.util.Timeout
import com.andrew_ya.ab.domain.{Lot, LotRequest, Lots, Orders}
import akka.pattern.ask

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

trait ServiceApi extends ClientService with LotsService {

//  import LotsServiceActor._
  import ClientServiceActor._

  def createClientsService(): ActorRef
  def createLotsService(): ActorRef


  implicit def executionContext: ExecutionContext
  implicit def requestTimeout: Timeout

  lazy val clientsService = createClientsService()
  lazy val lotsService = createLotsService()


  override def createLot(request: LotRequest): Future[Lot] = ???

  override def getActiveOrders(clientId: Long): Future[Option[Orders]] = ???

  override def findLotsByPrice(price: Double): Future[Option[Lots]] = ???

  override def getOrders(lotId: Long): Future[Option[Orders]] = ???
}

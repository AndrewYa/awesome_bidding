package com.andrew_ya.ab.services

import com.andrew_ya.ab.domain.{Lot, LotRequest, Lots, Orders}

import scala.concurrent.Future

trait LotsService {

  def createLot(request: LotRequest): Future[Lot] // 1
  def findLotsByPrice(price: Double): Future[Option[Lots]] // 2
  def getOrders(lotId: Long): Future[Option[Orders]] // 5

}
